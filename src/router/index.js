import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import TableFirst from "@/table/TableFirst"
import ThirdExample from "@/table/ThirdExample"
import ForthExample from "@/table/ForthExample"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path:"/table",
      name:"TableFirst",
      component:TableFirst
    },
    {
      path:"/third-example",
      name:"ThirdExample",
      component:ThirdExample
    },
    {
      path:"/forth-example",
      name:"ForthExample",
      component:ForthExample
    }
  ]
})
